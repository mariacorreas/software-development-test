%% PRE-PROCESSING OF AN ECG SIGNAL
% Maria Correas Garcia - 05/04/2022

%   WORKFLOW:
%   1. Find disconnected leads 
%      (leadStatus = 1 if is connected / leadStatus = 0 if is disconnected)
%   2. Recording spikes removal
%   3. Low-pass filter (40 Hz)
%   4. Powerline interference filtering (50 and 100 Hz)
%   5. Baseline wander (1 Hz) and offset removal
 
clear all
close all
clc

%Load the data
load("ecgConditioningExample.mat");
t = 0 : 1/fs : (size(ecg, 1) - 1)/fs;

%Visualize the data
figure; set(gcf, 'color', 'white');
for i=1:size(ecg,2)
    subplot(6,1,i); plot(t,ecg(:,i))
    xlabel('Time (s)'); title(['ECG raw (Channel ' num2str(i) ')'])
end

%% Find disconnected leads
for i=1:size(ecg,2)
    if mean(ecg(:,i))==0
        leadStatus(i) = 0; %Disconnected
    else
        leadStatus(i) = 1; %Connected
    end
end

%% Recording spikes removal
ecg_preprocessed1 = ecg;
for i=1:size(ecg,2)
    if leadStatus(i) == 1 %If the channel is connected

        %Detection of the spikes
        threshold = 10*median(ecg(:,i));
        for j = 1:size(ecg,1)
            if abs(ecg(j,i)) > threshold
                spikes(j) = 1; %There is a spike
            else
                spikes(j) = 0; %There is not a spike
            end
        end

        %Calculate the new values by interpolation
        ecg2 = []; t2 = []; t3 = [];
        for j =1:length(spikes)
            if spikes(j) == 0 %New vectors removing the spikes
                ecg2 = [ecg2 ecg(j,i)];
                t2 = [t2 t(j)];
            else
                t3 = [t3 t(j)]; %Time values with spikes
            end
        end
        ecg3 = interp1(t2,ecg2,t3); 

        %Add the new values to the signal
        n = 1;
        for j=1:length(spikes)
            if spikes(j) == 1 %There was a spike
                ecg_preprocessed1(j,i) = ecg3(n);
                n = n+1;
            end
        end
        
    end
end
clearvars ecg3; clearvars t2; clearvars ecg2; clearvars t3; 
clearvars spikes; clearvars n;

%Visualize the data
figure; set(gcf, 'color', 'white');
for i=1:size(ecg,2)
    subplot(6,1,i); plot(t,ecg_preprocessed1(:,i))
    xlabel('Time (s)'); title(['ECG spikes removed (Channel ' num2str(i) ')'])
end

%% Low-pass filter
Wn = 40/(fs/2); n=1;
[a,b] = butter(n, Wn, 'low');
ecg_preprocessed2 = filtfilt(a, b, ecg_preprocessed1);

%Visualize the data
tiempo = t(1:size(ecg_preprocessed2,1));
figure; set(gcf, 'color', 'white');
for i=1:size(ecg,2)
    subplot(6,1,i); plot(tiempo,ecg_preprocessed2(:,i))
    xlabel('Time (s)'); title(['ECG - Low-pass filtered (Channel ' num2str(i) ')'])
end

%% Powerline interference filtering

%50 Hz
w0 = 50/(fs/2);  
bw = w0/35;              
[num,den] = iirnotch(w0,bw);
ecg_preprocessed3 = filtfilt(num, den, ecg_preprocessed2);

%100 Hz
w0 = 100/(fs/2);  
bw = w0/35;              
[num,den] = iirnotch(w0,bw);
ecg_preprocessed4 = filtfilt(num, den, ecg_preprocessed3);

%Visualize the data
tiempo = t(1:size(ecg_preprocessed4,1));
figure; set(gcf, 'color', 'white');
for i=1:size(ecg,2)
    subplot(6,1,i); plot(tiempo,ecg_preprocessed4(:,i))
    xlabel('Time (s)'); title(['ECG - Powerline interference filtering (Channel ' num2str(i) ')'])
end

%% Baseline wander and offset removal

%High-pass filter
Wn_high = 1/(fs/2); n_high=1;
[c,d] = butter(n_high, Wn_high, 'high');
ecg_preprocessed5 = filtfilt(c, d, ecg_preprocessed4);

%Offset removal
ecg_preprocessed6 = [];
for i=1:size(ecg_preprocessed5,2)
    if leadStatus(i)==1 %The lead is connected
        lead = ecg_preprocessed5(:,i);
        y = linspace(ecg_preprocessed5(1,i),ecg_preprocessed5(end,1),length(lead));
        ecg_preprocessed6(:,i) = lead - y';
    end
end

%Visualize the data
tiempo = t(1:size(ecg_preprocessed6,1));
figure; set(gcf, 'color', 'white');
for i=1:size(ecg,2)
    subplot(6,1,i); plot(tiempo,ecg_preprocessed6(:,i))
    xlabel('Time (s)'); title(['ECG - Baseline wander and offset removal (Channel ' num2str(i) ')'])
end

